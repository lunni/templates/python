{% if cookiecutter.framework == 'FastAPI' -%}
from fastapi import FastAPI

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}
{% elif cookiecutter.framework == 'Flask' -%}
from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"
{% endif -%}
