# `lunni/templates/python`

This is a modern template that can be used to initiate a Python project with
all the necessary tools for development, testing, and deployment. It supports
the following features:

- [Poetry](https://python-poetry.org/) for dependency management
- Containerization with [Docker](https://www.docker.com/)
- CI/CD with [GitHub Actions](https://github.com/features/actions) or [GitLab CI](https://docs.gitlab.com/ee/ci/)
- Pre-commit hooks with [pre-commit](https://pre-commit.com/)
- Code quality with [black](https://pypi.org/project/black/), [ruff](https://github.com/charliermarsh/ruff), [mypy](https://mypy.readthedocs.io/en/stable/), and [deptry](https://github.com/fpgmaas/deptry/)
- Testing and coverage with [pytest](https://docs.pytest.org/en/7.1.x/) and [codecov](https://about.codecov.io/)
- Compatibility testing for multiple versions of Python with [Tox](https://tox.wiki/en/latest/)

---


## Quickstart

On your local machine, navigate to the directory in which you want to
create a project directory, and run the following command:

``` bash
lunni create python
```

Create a repository on your Git hosting, and then run the following commands:

``` bash
cd <project_name>
git init -b main
git add .
git commit -m "Init commit"
git remote add origin git@github.com:<username>/<project_name>.git
git push -u origin main
```

Finally, install the environment and the pre-commit hooks with:

```bash
make install
```

You are now ready to start development on your project! The CI/CD pipeline will
be triggered when you open a pull request, merge to main, or when you create a
new release.


## Acknowledgements

This template is based on [cookiecutter-poetry](https://fpgmaas.github.io/cookiecutter-poetry/)
by [Florian Maas](https://github.com/fpgmaas), but optimized for web
applications instead of Python libraries. cookiecutter-poetry is partially
based on [Audrey Feldroy](https://github.com/audreyfeldroy)'s great
[cookiecutter-pypackage](https://github.com/audreyfeldroy/cookiecutter-pypackage)
repository.
